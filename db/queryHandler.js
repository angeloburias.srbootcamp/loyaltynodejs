
const mysql = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'loyalty'
});


module.exports.queryHandler = {
    receiver: (query) => {
        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                err == true ? reject(err) : resolve(result);
            });
        });
    }
}

