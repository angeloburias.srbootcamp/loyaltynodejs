var express = require('express');
const { authController } = require('../src/Auth/controller/authController');
const { authService } = require('../src/Auth/service/authService');
var router = express.Router();
const bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const config = process.env;
require("dotenv").config();

/* GET login page. */
router.put('/login', async (req, res) => {
    const body = req.body;
    const result = await authController.login(body)
        .catch(err => { return { err } });

    const [user] = await authController.findUser(body);
    if (user && (await bcrypt.compare(body.password, user.password))) {
        const token = jwt.sign(
            { username: body.username },
            config.JWT_KEY,
            {
                expiresIn: "2h",
            }
        );
        if (result.length != 0) {
            res.status(200).send({
                message: "You successfully login",
                token: token
            })
        }
    } else {
        res.status(404).send({
            message: "Invalid Login Credentials"
        })
    }
});

module.exports = router;
