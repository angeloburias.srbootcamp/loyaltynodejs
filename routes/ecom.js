var express = require('express');
const { ecomController } = require('../src/Ecom/controller/ecomController');
const { pointController } = require('../src/Point/controller/pointController');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
    res.send('User Balance');
});

router.post('/addOrderItem', async (req, res) => {
    const body = req.body;
    const result = await ecomController.Create(body);

    if (result == 1) {
        res.json({
            message: 'Order added'
        });
    } else if (result == 2) {
        res.json({
            message: 'Dont have enough points'
        });
    } else {
        res.json({
            message: 'User doesnt exist'
        });
    }

});

router.get('/list', async (req, res) => {
    const result = await ecomController.List()
        .catch(err => { return { err } });

    if (!result || result['err'])
        return res.status(400)
            .send({ message: "Something went wrong" + result });

    if (result.length != 0) {
        res.json({
            message: 'Order item found',
            result: result
        });
    } else { res.send("Order item not found"); }

});

router.post('/add', async (req, res) => {
    const body = req.body;
    const result = await pointController.Create(body);
    res.json({
        message: 'Added User balance'
    });
});

router.get('/userbalance/:id', async (req, res) => {
    const { id } = req.params;
    const result = await pointController.FindUserBalance(id)
        .catch(err => { return { err } });

    if (!result || result['err'])
        return res.status(400)
            .send({ message: "Something went wrong" + result });

    if (result.length != 0) {
        res.json({
            message: 'Users Balance found',
            result: result
        });
    } else { res.send("Users Balance not found"); }

});

router.get('/findUser/:id', async (req, res) => {
    const { id } = req.params;
    const result = await pointController.FindUser(id)
        .catch(err => { return { err } });

    if (!result || result['err'])
        return res.status(400)
            .send({ message: "Something went wrong" + result });

    if (result.length != 0) {
        res.json({
            message: 'Users found',
            result: result
        });
    } else { res.send("Users not found"); }

});
module.exports = router;
