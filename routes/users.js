var express = require('express');

const { authController } = require('../src/Auth/controller/authController');
const { userController } = require('../src/Users/controller/userController');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('Users');
});

router.post('/register', async (req, res) => {
  const body = req.body;
  const result = await userController.Register(body);

  const auth = await authController.register(body, result.insertId);

  res.json({
    message: 'SUCCESS',
    result: result,
    auth: auth,
    id: body.id
  });

});

router.get('/list', async (req, res) => {
  const result = await userController.List()
    .catch(err => { return { err } });

  if (!result || result['err'])
    return res.status(400)
      .send({ message: "Something went wrong" + result });

  if (result.length != 0) {
    res.json({
      message: 'Users found',
      result: result
    });
  } else { res.send("User not found"); }

});

module.exports = router;
