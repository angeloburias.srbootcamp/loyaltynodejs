var express = require('express');
const { rewardController } = require('../src/Reward/controller/rewardController');
const { rewardService } = require('../src/Reward/service/rewardService');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
    res.send('Rewards');
});

router.post('/add', async (req, res) => {
    const body = req.body;
    const result = await rewardController.Create(body)
        .catch(err => err);
    if (result) {
        res.status(200).send({
            message: "Added Rewards success",
            result: result
        });
    } else {
        res.send({
            message: "Added Rewards success",
            result: result
        });
    }

});

router.patch('/update/:id', async (req, res) => {
    const { id } = req.params;
    const body = req.body;
    const result = await rewardController.Update(id, body)
        .catch(err => err);
    if (result) {
        res.status(200).send({
            message: "Update Rewards success",
            result: result
        });
    } else {
        res.send({
            message: "Failed",
            result: result
        });
    }

});

router.patch('/delete/:id', async (req, res) => {
    const { id } = req.params;
    const result = await rewardController.Delete(id)
        .catch(err => err);
    if (result) {
        res.status(200).send({
            message: "Deleted successfully",
            result: result
        });
    } else {
        res.send({
            message: "Failed to delete",
            result: result
        });
    }

});



router.patch('/redeemReward/', async (req, res) => {
    const body = req.body;
    const result = await rewardController.RedeemReward(body)
        .catch(err => err);
    if (result == 1) {
        res.status(200).send({
            message: "Redeem reward successfully",
        });
    } else if (result == 2) {
        res.send({
            message: "User dont have sufficient points",
        });
    } else { res.send({ message: "Account doesnt exist" }); }

});

router.get('/list', async (req, res) => {
    const result = await rewardController.List()
        .catch(err => { return { err } });

    if (!result || result['err'])
        return res.status(400)
            .send({ message: "Something went wrong" + result });

    if (result.length != 0) {
        res.json({
            message: 'Users found',
            result: result
        });
    } else { res.send("User not found"); }

});

module.exports = router;
