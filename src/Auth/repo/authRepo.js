const { queryHandler } = require('../../../db/queryHandler');
let dateNow = Date.now().toString();
const bcrypt = require('bcryptjs');
const config = process.env;
require("dotenv").config();
module.exports.authRepo = {

    async login(data) {
        const enc = await bcrypt.hashSync(data.password);

        const query = `
        SELECT COUNT(username) as User 
        FROM tbl_auth WHERE tbl_auth.username='${data.username}' 
        AND tbl_auth.password='${enc}'
        `;
        return await queryHandler.receiver(query);
    },

    async findUser(data) {
        const query = `
        SELECT username, password
        FROM tbl_auth WHERE username = '${data.username}'`;
        return await queryHandler.receiver(query);
    },

    async create(user_id, data) {
        const enc = await bcrypt.hashSync(data.password);
        const query = `
            INSERT INTO tbl_auth(username, password, date_created, date_delete, date_updated, user_id) 
            VALUES('${data.username}', '${enc}', '${dateNow}', '${dateNow}', '${dateNow}','${user_id}')
        `;
        return await queryHandler.receiver(query);
    },
    list() {
        const query = `SELECT * FROM tbl_auth`;
        return queryHandler.receiver(query);
    },
    update(data, id) {
        const query = `
            UPDATE tbl_auth 
            SET username=${data.username},  
            password=${data.password}
            WHERE id=${id}`;
        queryHandler.receiver(query);
    },
    delete(id) {
        let DateNow = Date.now();
        const query = `
            UPDATE SET
            tbl_auth.date_deleted=${DateNow} 
            FROM tbl_auth
            WHERE id=${id}
        `;
        queryHandler.receiver(query);
    }
}