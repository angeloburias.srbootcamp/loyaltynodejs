const { queryHandler } = require('../../../db/queryHandler');
let dateNow = Date.now().toString();
const bcrypt = require('bcryptjs');
const { authRepo } = require('../repo/authRepo');
const config = process.env;
require("dotenv").config();
module.exports.authService = {

    async login(data) {
        return authRepo.login(data);
    },
    async findUser(data) {
        return authRepo.findUser(data);
    },
    async create(user_id, data) {
        return authRepo.create(user_id, data);
    },
    list() {
        const query = `SELECT * FROM tbl_auth`;
        return queryHandler.receiver(query);
    },
    update(data, id) {
        const query = `
            UPDATE tbl_auth 
            SET username=${data.username},  
            password=${data.password}
            WHERE id=${id}`;
        queryHandler.receiver(query);
    },
    delete(id) {
        let DateNow = Date.now();
        const query = `
            UPDATE SET
            tbl_auth.date_deleted=${DateNow} 
            FROM tbl_auth
            WHERE id=${id}
        `;
        queryHandler.receiver(query);
    }
}