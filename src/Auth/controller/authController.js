const { authService } = require("../../Auth/service/authService")

module.exports.authController = {

    login: async (data) => {
        return await authService.login(data);
    },
    findUser: async (data) => {
        test = await authService.findUser(data);
        return test;
    },
    register: async (user_id, data) => {
        return await authService.create(user_id, data);
    }

}