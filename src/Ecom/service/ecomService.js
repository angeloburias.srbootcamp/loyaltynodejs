const { ecomRepo } = require('../repo/ecomRepo');
const { pointService } = require('../../Point/service/pointService');
const { rewardService } = require('../../Reward/service/rewardService');

let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);
let val = null;
module.exports.ecomService = {
    async create(data) {
        const [person] = await pointService.findByID(data.user_id);
        const productID = data.id;
        const [userPoints] = await pointService.findUserBalance(data.user_id);
        const [productsPointReward] = await rewardService.checkProductPoints(productID);
        if (person.count > 0) {
            var balance = userPoints.CurrentBalance + productsPointReward.points;
            await ecomRepo.addOrder(data);
            pointService.update(data.user_id, balance);
            return 1;
        } else { return 2; }
    },
    async list() {
        return await ecomRepo.list();
    },
    async update(data, id) {
        return await ecomRepo.update(id, data)
    },
    async delete(id) {
        return await ecomRepo.delete(id);
    }
}