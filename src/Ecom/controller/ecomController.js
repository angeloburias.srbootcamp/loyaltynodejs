const { ecomService } = require('../service/ecomService');

module.exports.ecomController = {
    Create: async (data) => {
        return await ecomService.create(data);
    },
    Update: async (id, data) => {
        return await ecomService.update(id, data);
    },
    List: async () => {
        return await ecomService.list();
    },
    Delete: async (id) => {
        return await ecomService.delete(id);
    }
}
