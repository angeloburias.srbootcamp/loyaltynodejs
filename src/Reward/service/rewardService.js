const { pointService } = require('../../Point/service/pointService');
const { rewardRepo } = require('../repo/rewardRepo');
module.exports.rewardService = {

    async redeemReward(data) {
        const [person] = await pointService.findByID(data.user_id);
        const productID = data.id;
        if (person.count > 0) {
            const [userPoints] = await pointService.findUserBalance(data.user_id);
            const [productsPointReward] = await this.checkProductPoints(productID);
            if (userPoints.CurrentBalance > productsPointReward.points) {
                var balance = userPoints.CurrentBalance - productsPointReward.points;
                await pointService.update(data.user_id, balance);
                rewardRepo.addRewardTrans(data);
                return 1;
            } else {
                return 2;
            }
        } else { return 3; }
    },
    async create(data) {
        return await rewardRepo.create(data);
    },
    async list() {
        return await rewardRepo.list();
    },
    async update(id, data) {
        return await rewardRepo.update(id, data);
    },
    async delete(id) {
        return await rewardRepo.delete(id);
    },
    async checkProductPoints(id) {
        return await rewardRepo.checkPointOfReward(id);
    }
}