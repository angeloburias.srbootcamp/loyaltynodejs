const { rewardService } = require('../service/rewardService');

module.exports.rewardController = {
    RedeemReward: async (data) => {
        return await rewardService.redeemReward(data);
    },
    Create: async (data) => {
        return await rewardService.create(data);
    },
    Update: async (id, data) => {
        return await rewardService.update(id, data);
    },
    List: async () => {
        return await rewardService.list();
    },
    Delete: async (id) => {
        return await rewardService.delete(id);
    }
}
