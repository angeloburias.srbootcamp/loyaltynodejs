const { queryHandler } = require('../../../db/queryHandler');
let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);
module.exports.rewardRepo = {
    create(data) {
        const query = `INSERT INTO tbl_rewards_product(rewards_name, points, date_created) 
            VALUES('${data.rewards_name}', '${data.points}', '${date}')`;
        return queryHandler.receiver(query);
    },
    addRewardTrans(data) {
        const query = `INSERT INTO tbl_rewards_trans(item_id, user_id) 
            VALUES('${data.id}', '${data.user_id}')`;
        return queryHandler.receiver(query);
    },
    list() {
        const query = `SELECT * FROM tbl_rewards_product`;
        return queryHandler.receiver(query);
    },
    update(id, data) {
        const query =
            `UPDATE tbl_rewards_product 
                SET rewards_name='${data.rewards_name}',  
                points='${data.points}',               
                date_updated='${date}'
                WHERE id='${id}'`;
        return queryHandler.receiver(query); ``
    },
    checkPointOfReward(id) {
        const query = `
            SELECT SUM(points) as points 
            FROM tbl_rewards_product
            WHERE id='${id}' 
        `
        return queryHandler.receiver(query);
    },
    delete(id) {
        const query = `
        UPDATE tbl_rewards_product
        SET date_delete='${date}'
        WHERE id='${id}'
        `;
        return queryHandler.receiver(query);
    }
}