const { queryHandler } = require('../../../db/queryHandler');
let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);
module.exports.pointRepo = {
    create(data) {
        const query = `INSERT INTO tbl_points(user_balance, user_id, date_created) 
            VALUES('${data.user_balance}', '${data.user_id}', '${date}')`;
        return queryHandler.receiver(query);
    },
    list() {
        const query = `SELECT * FROM tbl_points`;
        return queryHandler.receiver(query);
    },
    findByID(id) {
        const query = `SELECT Count(ups.id) as count, tu.firstname, tu.lastname, ups.user_balance 
        FROM tbl_points as ups INNER JOIN tbl_users tu ON tu.id = ups.id
         WHERE user_id ='${id}'`;
        return queryHandler.receiver(query);
    },
    findUserBalance(id) {
        const query = `SELECT SUM(user_balance) as CurrentBalance
         FROM tbl_points WHERE user_id ='${id}'`;
        return queryHandler.receiver(query);
    },
    update(id, data) {
        const query = `UPDATE tbl_points 
                SET user_balance='${data}',  
                date_updated='${date}'
                WHERE id='${id}'`;
        return queryHandler.receiver(query);
    },

    delete(id) {
        const query = `UPDATE SET tbl_users.date_delete='${date}' FROM tbl_users WHERE id='${id}'`;
        return queryHandler.receiver(query);
    }
}