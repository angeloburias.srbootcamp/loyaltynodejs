const { pointService } = require('../service/pointService');


module.exports.pointController = {
    Create: async (data) => {
        return await pointService.create(data);
    },
    Update: async (id, data) => {
        return await pointService.update(id, data);
    },
    List: async () => {
        return await pointService.list();
    },
    FindUser: async (id) => {
        return await pointService.findByID(id);
    },
    FindUserBalance: async (id) => {
        return await pointService.findUserBalance(id);
    },
    Delete: async (id) => {
        return await pointService.delete(id);
    }
}
