const { pointRepo } = require('../repo/pointRepo');

module.exports.pointService = {
    async create(data) {
        return await pointRepo.create(data);
    },
    async list() {
        return await pointRepo.list();
    },
    async findByID(id) {
        return await pointRepo.findByID(id);
    },
    async findUserBalance(id) {
        return await pointRepo.findUserBalance(id);
    },
    async update(id, data) {
        return await pointRepo.update(id, data);
    },
    async delete(id) {
        return await pointRepo.delete(id);
    }
}