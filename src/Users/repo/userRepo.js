const { queryHandler } = require('../../../db/queryHandler');
let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);

module.exports.userRepo = {
    login(data) {
        const query = `SELECT tbl_auth.username WHERE tbl_auth.username = '${data.token}'`;
        return queryHandler.receiver(query);
    },
    checkExistence(data) {
        const query = `SELECT COUNT(tbl_auth.username) WHERE tbl_auth.username = '${data.token}'`;
        return queryHandler.receiver(query);
    },
    create(data) {
        const query = `INSERT INTO tbl_users(firstname, lastname, email, phone_number, date_created) 
            VALUES('${data.firstname}', '${data.lastname}', '${data.email}', '${data.phone_number}', '${date}')`;
        return queryHandler.receiver(query);
    },
    list() {
        const query = `SELECT * FROM tbl_users`;
        return queryHandler.receiver(query);
    },
    update(id, data) {
        const query = `UPDATE tbl_users 
                SET firstName=${data.firstName},  
                lastName=${data.lastName},
                email=${data.email},
                phoneNumber=${data.phoneNumber},
                dateUpdated=${date}
                WHERE id=${id}`;
        queryHandler.receiver(query);
    },
    delete(id) {
        const query = `UPDATE SET tbl_users.date_deleted=${date} FROM tbl_users WHERE id=${id}`;
        queryHandler.receiver(query);
    }
}