
const { userService } = require('../../../src/Users/service/userService');

module.exports.userController = {
    Register: async (data) => {
        return await userService.create(data);
    },

    List: async () => {
        return await userService.list();
    },
    Login: async (data) => {
        return await userService.login(data);
    }
}
