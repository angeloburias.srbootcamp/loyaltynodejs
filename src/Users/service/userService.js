const { userRepo } = require('../repo/userRepo');

module.exports.userService = {
    async create(data) {
        return await userRepo.create(data);
    },
    async list() {
        return await userRepo.list();
    },
    async update(id, data) {
        return await userRepo.update(id, data);
    },
    async delete(id) {
        return await userRepo.delete(id);
    }
}